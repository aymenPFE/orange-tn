from django.shortcuts import redirect, render
from django.contrib.auth import get_user_model,login,logout,authenticate
from django.contrib.auth.decorators import login_required,permission_required

User= get_user_model()


def index (request):
  return render(request, 'accounts/index.html')


def signup (request):
    if request.method == 'POST':
      username= request.POST.get('username')
      password= request.POST.get('password')
      first_name= request.POST.get('first name')
      last_name=request.POST.get('last name')
      email=request.POST.get('email adress')
      user = User.objects.create_user(username=username , password=password ,
                                      first_name=first_name, last_name=last_name,
                                      email=email)
  

  
      login(request, user)    
      return redirect("services:home") 
    return render(request, 'accounts/signup.html')






def logout_user(request):
    logout(request)
    return redirect("accounts:index")




def login_user (request):
  
    if request.method=="POST":
      username= request.POST.get('username')
      password= request.POST.get('password')
      user =authenticate(username=username, password=password)
      if user:
        login(request,user)
        return redirect("accounts:index")
          
    return render(request, 'accounts/login.html')    


