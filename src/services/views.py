from django.shortcuts import render
from django.views.generic import ListView ,DetailView, CreateView , UpdateView , DeleteView
from django.urls import reverse_lazy
from .models import Service
from .forms import ServiceForm
from django.contrib.auth.decorators import login_required , user_passes_test 
from django.utils.decorators import method_decorator
# Create your views here.

from django.contrib.auth.models import Group
def home(request):
    return render(request, 'homeapp.html')


class HomeView(ListView):
    model = Service
    template_name = 'services\home.html'
    context_object_name = 'services'
    ordering = ['-id']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Home'
        return context

class ServiceDetailView(DetailView):
    model = Service
    template_name = 'services\detail.html'
    context_object_name = 'service'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Detail'
        return context

@login_required
@user_passes_test(lambda u: u.groups.filter(name='admin').exists())
class ServiceCreateView(CreateView):
    model = Service
    form_class = ServiceForm
    template_name = 'services\\create.html'
    success_url = reverse_lazy('services:home')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Create'
        return context

        
class ServiceUpdateView(UpdateView):
    model = Service
    template_name = 'services\\update.html'
    form_class = ServiceForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Update'
        return context
    
class ServiceDeleteView(DeleteView):
    model = Service
    template_name = 'services\\delete.html'
    success_url = reverse_lazy('services:home')
    context_object_name = 'service'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete'
        return context
